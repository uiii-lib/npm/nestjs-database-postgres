import { ConfigService } from '@uiii-lib/nestjs-config';

import { DatabaseConfig } from '../src/database.config';
import { DatabaseService } from '../src/database.service';

jest.mock('@uiii-lib/nestjs-config');

describe("Database service", () => {
	let pgClient: {query: jest.Mock};
	let databaseService: DatabaseService;

	beforeEach(() => {
		process.env.DB_MIGRATIONS_DIR = "/var/tmp/migrations";
		const configService = new ConfigService();

		pgClient = {
			query: jest.fn().mockReturnValue({rows: []})
		};

		const databaseConfig = new DatabaseConfig(configService);
		databaseService = new DatabaseService(pgClient as any, databaseConfig);
	})

	describe("query", () => {
		it("should query for multiple rows", async () => {
			const nativeSql = 'select * from "table" where "prop" = $1';
			const nativeBindings = [5];

			const rows = [{a: 5, b: "x"}, {a: 5, b: "y"}, {a: 5, b: "z"}];
			pgClient.query.mockReturnValue({rows});

			const result = await databaseService.query(query => query.from('table').where('prop', 5));

			expect(pgClient.query).toHaveBeenCalledWith(nativeSql, nativeBindings);
			expect(result).toStrictEqual(rows);
		});

		it("should accept query builder", async () => {
			const nativeSql = 'select * from "table" where "prop" = $1';
			const nativeBindings = [5];

			const query = databaseService.build().from('table').where('prop', 5);

			await databaseService.query(query);

			expect(pgClient.query).toHaveBeenCalledWith(nativeSql, nativeBindings);
		});

		it("should accept query builder callback", async () => {
			const nativeSql = 'select * from "table" where "prop" = $1';
			const nativeBindings = [5];

			await databaseService.query(query => query.from('table').where('prop', 5));

			expect(pgClient.query).toHaveBeenCalledWith(nativeSql, nativeBindings);
		});

		it("should accept raw SQL query", async () => {
			const querySql = 'SELECT FROM table WHERE ?? = ?';
			const queryBindings = ['prop', 5];

			const nativeSql = 'SELECT FROM table WHERE "prop" = $1';
			const nativeBindings = [5];

			await databaseService.query(query => query.raw(querySql, queryBindings));

			expect(pgClient.query).toHaveBeenCalledWith(nativeSql, nativeBindings);
		});
	});

	describe("queryOne", () => {
		it("should query for single row", async () => {
			const nativeSql = 'select * from "table" where "a" = $1';
			const nativeBindings = [5];

			const rows = [{a: 5, b: "x"}, {a: 5, b: "y"}, {a: 5, b: "z"}];
			pgClient.query.mockReturnValue({rows});

			const result = await databaseService.queryOne(query => query.from('table').where('a', 5));

			expect(pgClient.query).toHaveBeenCalledWith(nativeSql, nativeBindings);
			expect(result).toStrictEqual(rows[0])
		});
	})

	describe("execute", () => {
		it("should execute query without result", async () => {
			const nativeSql = 'truncate "table" restart identity';

			pgClient.query.mockReturnValue({rows: [{}]});

			const result = await databaseService.execute(query => query.from('table').truncate());

			expect(pgClient.query).toHaveBeenCalledWith(nativeSql, []);
			expect(result).toBeUndefined();
		});
	})

	describe("insert", () => {
		it("should run insert query", async () => {
			const nativeSql = 'insert into "table" ("a", "b") values ($1, $2) returning *';
			const nativeBindings = [5, "str"];

			const obj = {
				a: 5,
				b: "str"
			}

			pgClient.query.mockReturnValue({rows: [obj]});

			const result = await databaseService.insert("table", obj);

			expect(pgClient.query).toHaveBeenCalledWith(nativeSql, nativeBindings);
			expect(result).toStrictEqual(obj);
		});
	})

	describe("update", () => {
		it("should run insert query", async () => {
			const nativeSql = 'update "table" set "a" = $1, "b" = $2 where ("a" = $3) returning *';
			const nativeBindings = [5, "str", 6];

			const obj = {
				a: 5,
				b: "str"
			}

			pgClient.query.mockReturnValue({rows: [obj]});

			const result = await databaseService.update("table", obj, query => query.where('a', 6));

			expect(pgClient.query).toHaveBeenCalledWith(nativeSql, nativeBindings);
			expect(result).toStrictEqual(obj);
		});
	})

	describe("upsert", () => {
		it("should run upsert query", async () => {
			const nativeSql = 'insert into "table" ("a", "b") values ($1, $2) on conflict ("a") do update set "a" = excluded."a", "b" = excluded."b" returning *';
			const nativeBindings = [5, "str"];

			const obj = {
				a: 5,
				b: "str"
			}

			pgClient.query.mockReturnValue({rows: [obj]});

			const result = await databaseService.upsert("table", obj, ["a"]);

			expect(pgClient.query).toHaveBeenCalledWith(nativeSql, nativeBindings);
			expect(result).toStrictEqual(obj);
		});
	})

	describe("remove", () => {
		it("should run delete query", async () => {
			const nativeSql = 'delete from "table" where ("a" = $1) returning *';
			const nativeBindings = [5];

			const obj = {
				a: 5,
				b: "str"
			}

			pgClient.query.mockReturnValue({rows: [obj, obj]});

			const result = await databaseService.remove("table", query => query.where("a", 5));

			expect(pgClient.query).toHaveBeenCalledWith(nativeSql, nativeBindings);
			expect(result).toStrictEqual([obj, obj]);
		});
	})

	describe("columns", () => {
		it("should return non-aliased table columns info", async () => {
			const nativeSql = 'select "column_name", "data_type" from "information_schema"."columns" where "table_schema" = $1 and "table_name" = $2';
			const nativeBindings = ["public", "table"];

			const rows = [{column_name: 'column1', data_type: 'int'}, {column_name: 'column2', data_type: 'text'}];
			pgClient.query.mockReturnValue({rows});

			const result = await databaseService.columns('table');

			expect(pgClient.query).toHaveBeenCalledWith(nativeSql, nativeBindings);
			expect(result).toStrictEqual(rows.map(row => ({...row, aliased_column_name: row.column_name})));
		});

		it("should return aliased table columns info", async () => {
			const nativeSql = 'select "column_name", "data_type" from "information_schema"."columns" where "table_schema" = $1 and "table_name" = $2';
			const nativeBindings = ["schema", "table"];

			const rows = [{column_name: 'column1', data_type: 'int'}, {column_name: 'column2', data_type: 'text'}];
			pgClient.query.mockReturnValue({rows});

			const result = await databaseService.columns('schema.table', 'alias');

			expect(pgClient.query).toHaveBeenCalledWith(nativeSql, nativeBindings);
			expect(result).toStrictEqual(rows.map(row => ({...row, aliased_column_name: `alias.${row.column_name}`})));
		});
	});
});
