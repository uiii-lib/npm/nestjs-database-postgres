# PostgreSQL database module for NestJS

PostgreSQL database module for NestJS application. Exports a service for convenient access to the database. Supports SQL migrations.

## Configuration

Required environment variables:

- `DB_HOST` - database host
- `DB_PORT` - database port
- `DB_NAME` - database name
- `DB_USER` - database user
- `DB_PASS` - database user password
- `DB_MIGRATIONS` (*optional*) - set to `true` to enable migrations (default `false`), see  [Migrations](#migrations)
- `DB_MIGRATIONS_DIR` (*optional* unless `DB_MIGRATIONS=true`) - path to a directory containing migration sql scripts
- `DB_MIGRATIONS_INIT` (*optional*) - set to `false` if you want to initialize database schema for migrations manually (default `true`)
- `DB_MIGRATIONS_SCHEMA` (*optional*) - migrations database table schema name (default `db`)
- `DB_MIGRATIONS_TABLE` (*optional*) - migrations database table name (default `migration`)

## Migrations

If the migrations are enabled (`DB_MIGRATIONS=true`) then this module will apply migrations from `DB_MIGRATIONS_DIR` directory. Each script should be named `YYYY-MM-DD-hh-mm-<some-name>.sql`.

> When the module is initialized it will create database schema for migrations if not exists.

See [Configuration](#configuration) and `DB_MIGRATIONS_*` env variables for more options.

## Usage

Register `@uiii-lib` package registry to npm

```
npm config set @uiii-lib:registry https://gitlab.com/api/v4/packages/npm/
```

Install the package

> Requires [`@uiii-lib/nestjs-config`](https://gitlab.com/uiii-lib/npm/nestjs-config) and [`@nestjs/graphql`](https://github.com/nestjs/graphql) as peer dependencies, so install them too

```
npm install --save @uiii-lib/nestjs-database-postgres
```

Add database module to `imports` of a module which you want to use it in.

```ts
import { Module } from '@nestjs/common';
import { DatabaseModule } from '@uiii-lib/nestjs-database-postgres';

@Module({
	imports: [
		DatabaseModule
	],
	...
})
export class SomeModule {}
```

Use in some module's service

```ts
import { Injectable } from '@nestjs/common';
import { DatabaseService } from '@uiii-lib/nestjs-database-postgres';

@Injectable()
export class SourceService {
	constructor(private dbService: DatabaseService) {}

	async getSomeEntity() {
		const rows = await this.dbService.query<SomeEntity>(this.dbService.build()
			.from('entity')
		);

		return rows.map(row => this.rowToEntity(row));
	}

	...
}
```

