import pg from 'pg';

import { Module } from '@nestjs/common';
import { ConfigModule } from '@uiii-lib/nestjs-config';

import { DatabaseConfig } from './database.config';
import { DatabaseService } from './database.service';
import { DatabaseResolver } from './database.resolver';

@Module({
	imports: [
		ConfigModule
	],
	providers: [
		DatabaseConfig,
		DatabaseService,
		DatabaseResolver,
		{
			provide: pg.Pool,
			useFactory: async (config: DatabaseConfig) => {
				console.log(config);
				const client = new pg.Pool({
					host: config.dbHost,
					port: config.dbPort,
					user: config.dbUser,
					password: config.dbPassword,
					database: config.dbName,
					query_timeout: 1000,
				});

				return client;
			},
			inject: [DatabaseConfig]
		}
	],
	exports: [
		DatabaseService
	]
})
export class DatabaseModule {}
