export interface MigrationRow {
	id: number;
	name: string;
	applied_at: Date;
}
