import fs from 'fs';
import path from 'path';
import pg, { QueryResultRow } from 'pg';
import knex, { Knex } from 'knex';
import { difference, sortBy } from 'lodash';

import { Injectable, OnModuleInit } from "@nestjs/common";

import { MigrationRow } from './model/db/migration.row';

import { DatabaseConfig } from './database.config';
import { OptionalId } from './model/optional-id';

export type QueryBuilder = Knex.QueryBuilder|Knex.Raw;
export type QueryBuilderCallback = (builder: Knex) => QueryBuilder;

export type SchemaBuilder = Knex.SchemaBuilder;

export interface RawQuery {
	sql: string;
	bindings: any[];
}

export interface ColumnRow {
	column_name: string;
	aliased_column_name: string;
	data_type: string;
}

@Injectable()
export class DatabaseService implements OnModuleInit {
	protected queryBuilder: Knex;

	constructor(private db: pg.Pool, private config: DatabaseConfig) {
		this.queryBuilder = knex({client: 'pg'});
	}

	async onModuleInit() {
		await this.initMigrations();
		await this.applyMigrations();
	}

	client() {
		return this.db;
	}

	build() {
		return this.queryBuilder;
	}

	async query<T extends QueryResultRow = any>(queryBuilder: QueryBuilder|QueryBuilderCallback): Promise<T[]> {
		if (typeof queryBuilder === "function") {
			queryBuilder = queryBuilder(this.build());
		}

		const {sql, bindings} = queryBuilder.toSQL().toNative();
		const result = await this.db.query<T>(sql, bindings as any[]);
		return result.rows;
	}

	async queryOne<T extends QueryResultRow = any>(query: QueryBuilder|QueryBuilderCallback): Promise<T|undefined> {
		const result = await this.query<T>(query);
		return result[0];
	}

	async queryRaw<T extends QueryResultRow = any>(rawQuery: RawQuery): Promise<T[]> {
		return this.query<T>(this.build().raw(rawQuery.sql, rawQuery.bindings));
	}

	async queryOneRaw<T extends QueryResultRow = any>(rawQuery: RawQuery): Promise<T|undefined> {
		return this.queryOne<T>(this.build().raw(rawQuery.sql, rawQuery.bindings));
	}

	async querySchema<T extends QueryResultRow = any>(query: SchemaBuilder): Promise<T|undefined> {
		const rawQueries = query.toSQL() as any as RawQuery[];

		let result: T|undefined;
		for (let rawQuery of rawQueries) {
			result = await this.queryOneRaw<T>(rawQuery);
		}

		return result;
	}

	async execute<T extends QueryResultRow = any>(query: QueryBuilder|QueryBuilderCallback): Promise<void> {
		await this.query<T>(query);
	}

	async insert<I extends {id: any} = any, R extends QueryResultRow = I>(table: string, valuesByColumn: OptionalId<I>) {
		const row = await this.queryOne<R>(this.build()
			.table(table)
			.returning('*')
			.insert(valuesByColumn)
		);

		return row!;
	}

	async update<T extends QueryResultRow = any>(table: string, valuesByColumn: Partial<T>, where: Knex.QueryCallback) {
		return await this.queryOne<T>(this.build()
			.table(table)
			.returning('*')
			.where(where)
			.update(valuesByColumn)
		);
	}

	async upsert<T extends {id: any} = any>(table: string, valuesByColumn: OptionalId<T>, conflictColumns: string[]) {
		const row = await this.queryOne<T>(this.build()
			.from<any>(table)
			.returning('*')
			.insert(valuesByColumn)
			.onConflict(conflictColumns)
			.merge()
		);

		return row!;
	}

	async remove<T extends QueryResultRow = any>(table: string, where: Knex.QueryCallback) {
		return await this.query<T>(this.build()
			.from(table)
			.where(where)
			.del()
			.returning('*')
		);
	}

	async columns(tableName: string, tableAlias?: string) {
		const [table, schema = 'public'] = tableName.split('.').reverse();

		let rows = await this.query<ColumnRow>(this.build()
			.from('information_schema.columns')
			.select('column_name', 'data_type')
			.where('table_schema', schema)
			.where('table_name', table)
		);

		rows = rows.map(row => ({
			...row,
			aliased_column_name: tableAlias ? `${tableAlias}.${row.column_name}` : row.column_name
		}))

		return rows;
	}

	async initMigrations() {
		if (!this.config.migrationsEnabled || !this.config.initMigrationsDb) {
			return;
		}

		const table = await this.querySchema(this.build().schema
			.withSchema(this.config.migrationsDbSchema)
			.hasTable(this.config.migrationsDbTable) as any
		);

		if (table) {
			return;
		}

		await this.querySchema(this.build().schema.createSchemaIfNotExists(this.config.migrationsDbSchema));
		await this.querySchema(this.build().schema
			.withSchema(this.config.migrationsDbSchema)
			.createTable(this.config.migrationsDbTable, table => {
				table.increments().primary();
				table.text("name").notNullable().unique();
				table.timestamp("applied_at", {useTz: true}).defaultTo(this.build().fn.now());
			})
		);
	}

	async applyMigrations() {
		if (!this.config.migrationsEnabled || !this.config.migrationsDir) {
			return;
		}

		const migrationFiles = fs.readdirSync(this.config.migrationsDir).filter(f => f.match(/.*\.sql$/));

		const migationsTable = `${this.config.migrationsDbSchema}.${this.config.migrationsDbTable}`;
		const migrations = await this.query<MigrationRow>(this.build().from(migationsTable));

		const notAppliedFilenames = difference(migrationFiles, migrations.map(m => m.name));
		const latestAppliedFilename = migrations[migrations.length - 1]?.name;

		await this.db.query('BEGIN');

		try {
			for (let migrationFilename of sortBy(notAppliedFilenames)) {
				if (latestAppliedFilename && migrationFilename < latestAppliedFilename) {
					console.warn(`WARNING: migration ${migrationFilename} not applied, but is older than the latest applied migration.`);
					continue;
				}

				console.log(`applying migration '${migrationFilename}'`);
				await this.db.query(fs.readFileSync(path.join(this.config.migrationsDir, migrationFilename), 'utf-8'));

				await this.insert<MigrationRow>(migationsTable, {
					name: migrationFilename,
					applied_at: new Date()
				});
			}

			await this.db.query('COMMIT');
		} catch (e) {
			await this.db.query('ROLLBACK');
			throw e;
		}
	}
}
