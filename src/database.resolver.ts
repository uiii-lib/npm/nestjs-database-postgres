import { Resolver, Mutation } from '@nestjs/graphql';

import { DatabaseService } from './database.service';

@Resolver('Database')
export class DatabaseResolver {
	constructor(private databaseService: DatabaseService) {}

	@Mutation(returns => String)
	async applyMigrations() {
		await this.databaseService.applyMigrations();
		return "ok";
	}
}
