import { Injectable } from '@nestjs/common';

import { ConfigService } from '@uiii-lib/nestjs-config';

@Injectable()
export class DatabaseConfig {
	constructor(private config: ConfigService) {}

	get dbHost(): string {
		return this.config.get('DB_HOST', true)!;
	}

	get dbPort(): number {
		return parseInt(this.config.get('DB_PORT', true)!);
	}

	get dbName(): string {
		return this.config.get('DB_NAME', true)!;
	}

	get dbUser(): string {
		return this.config.get('DB_USER', true)!;
	}

	get dbPassword(): string {
		return this.config.get('DB_PASS', true)!;
	}

	get migrationsEnabled(): boolean {
		return this.config.get('DB_MIGRATIONS') === 'true';
	}

	get migrationsDir(): string|undefined {
		return this.config.get('DB_MIGRATIONS_DIR', this.migrationsEnabled);
	}

	get initMigrationsDb(): boolean {
		return this.config.get('DB_MIGRATIONS_INIT') !== 'false';
	}

	get migrationsDbSchema(): string {
		return this.config.get('DB_MIGRATIONS_SCHEMA') || 'db';
	}

	get migrationsDbTable(): string {
		return this.config.get('DB_MIGRATIONS_TABLE') || 'migration';
	}
}
